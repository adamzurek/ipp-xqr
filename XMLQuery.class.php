<?php

#XQR:xzurek13

/**
 * Aplikuje operace nad daty XML.
 */
class XMLQuery
{
    /**
     * @var SimpleXMLElement[]
     */
    private $data = array();

    /**
     * Načte XML a vybere ROOT element
     *
     * @param string $xml
     * @param string $from
     *
     * @throws BadInputFileFormatException
     */
    public function __construct ($xml, $from)
    {
        $sXml = @simplexml_load_string(mb_convert_encoding($xml, 'UTF-8'));           // načíst xml, @ kvůli errorům
        if ($sXml === false) {
            throw new BadInputFileFormatException('Vstupní soubor není validním XML dokumentem.');
        }

        $this->data = array($sXml);                     // načtené xml dát do pole

        if ($from[0] == '.') {                          // pokud se hledá podle atributu
            $from = '*' . $from;                        // může být jméno tagu jakékoliv
        }

        $all_from = Helper::findByEoA($this->data, $from);
        if ($from != 'ROOT' && isset($all_from[0])) {   // pokud se nehledá z rootu
            $this->data = $all_from[0];                 // vybrat pouze první výskyt
        } else {
            $this->data = $all_from;                    // jinak vše z rootu
        }
    }

    /**
     * Aplikuje výběr elementu nad daty.
     *
     * @param string $element
     *
     * @return XMLQuery
     */
    public function select ($element)
    {
        $this->data = Helper::findByEoA($this->data, $element);

        return $this;
    }

    /**
     * Aplikuje podmínku nad daty.
     *
     * @param $where
     *
     * @return XMLQuery
     *
     * @throws BadWhereException
     * @throws SearchInChildrenException
     */
    public function where ($where)
    {
        $w = null;
        $output = array();

        try {
            $w = new Where($where);
        } catch (BadWhereException $e) {    // syntaktická/sémantická chyba ve where
            throw $e;
        } catch (NoWhereException $e) {     // žádná podmínka není definována
            return $this;
        }

        foreach ($this->data as $data) {
            try {
                if ($w->apply($data)) {                 // pokud vyhovuje podmínce
                    $output[] = $data;                  // přidat do výsledného pole
                }
            } catch (SearchInChildrenException $e) {    // pokud má element podelementy
                throw $e;
            }
        }

        $this->data = $output;

        return $this;
    }

    /**
     * Srovná elementy podle zadaného elementu.
     *
     * @param string $by
     * @param string $sort
     *
     * @return XMLQuery
     */
    public function order ($by, $sort)
    {
        usort($this->data, Helper::sort($by, $sort));   // seřadit prvky

        foreach ($this->data as $k => &$el) {           // projít prvky a očíslovat
            $el->addAttribute('order', $k + 1);
        }
    }

    /**
     * @param int $limit
     *
     * @return XMLQuery
     */
    public function limit ($limit)
    {
        if ($limit > -1) {
            $this->data = array_slice($this->data, 0, $limit);
        }

        return $this;
    }

    /**
     * Převede data do XML
     *
     * @param bool $skipHead
     * @param string $root
     *
     * @return string
     */
    public function toXml ($skipHead, $root)
    {
        $xml = '';

        // projdu všechny elemnty a změním je na query, poté za sebe spojím
        foreach ($this->data as $child) {
            $xml .= $child->asXML();
        }

        // obalení do obalovacího tagu (parametr --root)
        if ($root) {
            $xml = '<' . $root . '>' . $xml . '</' . $root . '>';
        }

        // výpis XML hlavičky (parametr -n)
        if ($skipHead === false) {
            $xml = '<?xml version="1.0" encoding="utf-8"?>' . $xml;
        }

        return $xml;
    }
}

/**
 * Class BadInputFileFormatException
 * Špatný formát vstupního souboru.
 */
class BadInputFileFormatException extends InputFileNotFoundException
{
    /**
     * @var int
     */
    protected $code = 4;
}
