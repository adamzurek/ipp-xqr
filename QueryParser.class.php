<?php

#XQR:xzurek13

/**
 * Rozparsuje SQL na části
 *
 * @property string select
 * @property array where
 * @property int limit
 * @property string from
 * @property string order
 * @property string sort
 */
class QueryParser
{
    /**
     * @var string
     */
    public $select = '';

    /**
     * @var string
     */
    public $where;
    /**
     * @var int
     */
    public $limit = -1;
    /**
     * @var string
     */
    public $from = '*';
    /**
     * @var string
     */
    public $order = '';
    /**
     * @var string
     */
    public $sort = 'ASC';

    /**
     * Kontroluje syntaxi a sémantiku sql
     *
     * @param string $sql
     * @throws \BadSqlException
     */
    public function __construct ($sql)
    {
        $elOrAttr = '(?:(?:\w+)?(?:\.\w+)?)';   // může být element nebo atribut

        $correctSql = preg_match('~^\s*SELECT\s+(?P<SELECT>[a-zA-Z_][a-zA-Z0-9_\.-]*)(?:\s+LIMIT\s+(?P<LIMIT>\d+))?(?:\s+FROM(?:\s+(?P<FROM>' . $elOrAttr . '))?)(?:\s+WHERE\s+(?P<WHERE>.+))?(?:\s+ORDER BY\s+(?P<ORDER>' . $elOrAttr . ')(?:\s+(?P<sort>ASC|DESC)?))?\s*$~mU', $sql, $sqlMatches);

        // kontrola sql
        if (!$correctSql || (isset($sqlMatches['FROM']) && strcasecmp($sqlMatches['FROM'], 'ROOT') == 0) && strcmp($sqlMatches['FROM'], 'ROOT') != 0) {
            throw new BadSqlException('Syntaktická chyba v SQL.');
        }

        if ($sqlMatches['SELECT'][0] == '.') {                      // pokud není uveden název elementu, na kterém se hledá atribut
            $sqlMatches['SELECT'] = '*' . $sqlMatches['SELECT'];    // pak jsou to všechny tagy *
        }

        $this->select = $sqlMatches['SELECT'];  // není potřeba podmínka, protože je to povinná část SQL dotazu

        // strlen > 0 protože preg_match vracel "" při nenalezení LIMIT
        if (isset($sqlMatches['LIMIT']) && strlen($sqlMatches['LIMIT']) > 0 && $sqlMatches['LIMIT'] >= 0) {
            $this->limit = (int)$sqlMatches['LIMIT'];
        }
        // neprázdný
        if (!empty($sqlMatches['FROM'])) {
            // není root
            if ($sqlMatches['FROM'] != 'ROOT') {
                if ($sqlMatches['FROM'][0] === '.') {                   // pokud není uveden název elementu, na kterém se hledá atribut
                    $sqlMatches['FROM'] = '*' . $sqlMatches['FROM'];    // pak jsou to všechny tagy *
                }

                $this->from = $sqlMatches['FROM'];
            } else {    // ROOT => vyhledává od kořenového elementu
                $this->from = '*';
            }
        } else {        // prázdný FROM-ELM
            $this->from = null;
        }

        if (isset($sqlMatches['WHERE'])) {
            $this->where = $sqlMatches['WHERE'];
        }

        if (isset($sqlMatches['ORDER'])) {
            $this->order = $sqlMatches['ORDER'];
        }

        if (isset($sqlMatches['SORT'])) {
            $this->sort = $sqlMatches['SORT'];
        }
    }
}

class BadSqlException extends BadArgumentException
{
    /**
     * @var int
     */
    protected $code = 80;
}
