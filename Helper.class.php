<?php

#XQR:xzurek13

/**
 * Class Helper
 *
 * Obsahuje
 */
abstract class Helper
{
    /**
     * Porovná hodnoty $val1 a $val2 podle uvedeného $op operátoru.
     *
     * @param string $val1
     * @param string $op
     * @param string $val2
     *
     * @return bool
     */
    public static function compare ($val1, $op, $val2)
    {
        switch ($op) {
            case '>':
                return $val1 > $val2;
            case '<':
                return $val1 < $val2;
            case '=':
                return $val1 == $val2;
            case 'CONTAINS':
                return mb_strpos($val1, $val2) !== false;
        }

        return false;
    }

    /**
     * Zapíše chybu na stderr a skončí s daným návratovým kódem.
     *
     * @param \Exception $e
     *
     * @exits $e->getCode()
     */
    public static function error (Exception $e)
    {
        file_put_contents('php://stderr', $e->getMessage());
        exit($e->getCode());
    }

    /**
     * Vypíše nápovědu a skončí s návratovým kódem 0.
     *
     * @exits 0
     */
    public static function printHelp()
    {
        echo 'Použití:' . PHP_EOL;
        echo str_replace(__DIR__ . '/', '', __FILE__) . ' [parametry]' . PHP_EOL . PHP_EOL;
        echo 'Parametry:' . PHP_EOL;
        echo '--help                Vypíše tuto nápovědu. Nelze kombinovat s jinýmy parametry.' . PHP_EOL;
        echo '--input=filename      Zadaný vstupní soubor ve formátu XML' . PHP_EOL;
        echo '--output=filename     Zadaný výstuní soubor ve formátu XML s obsahem podle zadaného dotazu' . PHP_EOL;
        echo '--query="dotaz"       Zadaný dotaz v dotazovacím jazyce' . PHP_EOL;
        echo '--qf=filename         Dotaz v dotazovacím jazyce v externím textovém souboru (nelze kombinovat s --query)' . PHP_EOL;
        echo '-n                    NEgenerovat XML hlavičku na výstup' . PHP_EOL;
        echo '--root=element        Jméno párového kořenového elementu obalující výsledky. Implicitně bude vynechán.' . PHP_EOL;

        exit(0);
    }

    /**
     * Rozdělí tag (a/nebo atribut) na element a atribut.
     *
     * @param string $EoA
     * @return array
     */
    public static function parseEoA ($EoA)
    {
        $el = '*';
        $attr = '';

        $exploded = explode('.', $EoA);
        if (isset($exploded[0])) {
            $el = $exploded[0];

            if (isset($exploded[1])) {
                $attr = $exploded[1];
            }
        }

        return array($el, $attr);
    }

    /**
     * Seřadí dva prvky lexikograficky.
     *
     * @param $by
     * @param $sort
     *
     * @return callable
     */
    public static function sort ($by, $sort)
    {
        return function ($a, $b) use ($by, $sort) {
            $aEl = Helper::findByEoA($a, $by);
            $bEl = Helper::findByEoA($b, $by);

            if ($aEl && $bEl) {
                return ($sort == 'ASC' ? -1 : 1) * strcasecmp($aEl[0], $bEl[0]);
            } else {
                return 0;
            }
        };
    }


    /**
     * Hledá elementy podle názvu elementu (a) nebo atributu
     *
     * @param SimpleXMLElement[] $elements
     * @param string $tag
     *
     * @return SimpleXMLElement[]
     */
    public static function findByEoA ($elements, $tag)
    {
        $output = array();

        list($el, $attr) = Helper::parseEoA($tag);

        // prohledávat potomky
        foreach ($elements as $child) {
            if ($el == '*' || $child->getName() == $el) {   // hledat podle jména
                if ($attr) {                                // hledat podle atributu
                    $attrs = $child->attributes();

                    if (isset($attrs[$attr])) {             // nalezeno
                        array_push($output, $child);
                    } else if ($child->count()) {           // pokud má potomky tak hledat v nich
                        $output = array_merge($output, self::findByEoA($child, $tag));
                    }
                } else {                                    // nalezeno podle jména
                    array_push($output, $child);
                }
            } else if ($child->count()) {                   // nenalezeno => zkusím hledat dál v potomcích
                $output = array_merge($output, self::findByEoA($child, $tag));
            }
        }

        return $output;
    }
}
