<?php

#XQR:xzurek13

/**
 * Parsuje a aplikuje restrikci nad daty z XML. 
 */
class Where
{
    /**
     * @var string
     */
    private $column = '';

    /**
     * @var string
     */
    private $operator = '';

    /**
     * @var string
     */
    private $literal = '';

    /**
     * @var bool
     */
    private $not = false;

    /**
     * @param $where
     *
     * @throws BadWhereException
     * @throws NoWhereException
     */
    public function __construct ($where)
    {
        if (empty($where)) {
            throw new NoWhereException('Není zadán WHERE.');
        }

        if (substr_count($where, '(') == substr_count($where, ')')) {   // kontrola stejného počtu levých a pravých závorek
            $where = str_replace('(', '', $where);                      // odstranění levých závorek
            $where = str_replace(')', '', $where);                      // odstranění pravých závorek
        } else {
            throw new BadWhereException('Počet levých závorek je jiný než počet pravých závorek.');
        }

        $correctWhere = preg_match('~^\s*(?P<NOT>(?:(?:\s*NOT)*))\s*(?P<COLUMN>(?:[a-zA-Z\.]+))\s*(?P<OPERATOR>(?:[<=>]| CONTAINS ))\s*(?P<LITERAL>(?:(?:\".+\")|(?:[+-]?\d+)))\s*$~m', $where, $whereMatch);

        if (!$correctWhere) {
            throw new BadWhereException('Syntaktická chyba ve WHERE.');
        }

        $whereMatch['OPERATOR'] = trim($whereMatch['OPERATOR']);

        if ($whereMatch['OPERATOR'] == 'CONTAINS' && $whereMatch['LITERAL'][0] != '"') {
            throw new BadWhereException('Literál je číselného typu při použití operátoru CONTAINS.');
        }

        if (!empty($whereMatch['NOT'])) {
            $nots = substr_count($whereMatch['NOT'], 'NOT');    // spočítám kolikrát je zadáno NOT
            $this->not = $nots % 2 != 0;                        // NOT NOT => nebudu používat NOT
        }

        if ($whereMatch['COLUMN'][0] == '.') {                  // není zadáno jméno tagu
            $whereMatch['COLUMN'] = '*' . $whereMatch['COLUMN'];// přidat * pro vybrání všech tagů (XPath)
        }

        $this->column = $whereMatch['COLUMN'];
        $this->operator = $whereMatch['OPERATOR'];
        $this->literal = str_replace('"', '', $whereMatch['LITERAL']);
    }

    /**
     * Zkontroluje jestli daný element (s podelementy) odpovídá podmínce.
     *
     * @param SimpleXMLElement|SimpleXMLElement[] $elements
     *
     * @return bool
     */
    public function apply ($elements)
    {
        list($el, $attr) = Helper::parseEoA($this->column);

        try {
            $check = $this->check($elements, $el, $attr);   // zkontrolovat jestli platí podmínka
            if ($check !== null) {
                return $check;
            }
        } catch (SearchInChildrenException $e) {
            throw $e;
        }

        foreach ($elements as $child) {
            if ($child->count()) {                          // pokud má element potomky
                if ($this->apply($child)) {                 // pokud je podstrom úspěšný
                    if ($this->not) {                       // pokud je nastaven NOT, tak pokračuji v prohledávání sousedů
                        continue;
                    } else {                                // pokud není nastaven NOT
                        return true;                        // pak je aktuální uzel úspěšný
                    }
                } else if ($this->not) {                    // pokud není něco v podstromu úspěšné a je nastaven NOT
                    return false;                           // pak je aktuální uzel neúspěšný
                }
            }

            try {
                $check = $this->check($child, $el, $attr);  // zkontorlovat jestli platí podmínka
                if ($check !== null) {
                    return $check;
                }
            } catch (SearchInChildrenException $e) {
                throw $e;
            }
        }

        return $this->not;
    }


    /**
     * Zkontroluje jestli element $child, jména $el, případně atributu $attr odpovídá podmínce ve WHERE.
     *
     * @param SimpleXMLElement $child
     * @param string $el
     * @param string $attr
     *
     * @return null|bool
     *
     * @throws SearchInChildrenException
     */
    public function check ($child, $el, $attr)
    {
        if ($el == '*' || $child->getName() == $el) {   // pokud má element stejné jméno
            if ($attr) {                                // pokud hledám podle atributu
                $attrs = $child->attributes();

                if (isset($attrs[$attr])) {
                    $eq = Helper::compare($attrs[$attr], $this->operator, $this->literal);
                    if ($this->not && !$eq) {   // pokud je NOT a nerovnají se tak pokračovat s prohledáváním
                        return null;
                    }
                    return $this->not xor $eq;  // pokud mám NOT a nerovnají se nebo nemám not a rovnají se => true, jinak false
                }
            } else {                                    // nalezen hledaný element podle jména
                if ($child->count()) {                  // pokud má element potomky tak vrátit chybu 4
                    throw new SearchInChildrenException("Nelze porovnat element bez hodnoty.");
                }

                $eq = Helper::compare($child, $this->operator, $this->literal);
                if ($this->not && !$eq) {   // pokud je NOT a nerovnají se tak pokračovat s prohledáváním
                    return null;
                }
                return $this->not xor $eq;  // pokud mám NOT a nerovnají se nebo nemám not a rovnají se => true, jinak false
            }
        }

        return null;
    }
}

class BadWhereException extends BadSqlException {}

class NoWhereException extends Exception
{
    /**
     * @var int
     */
    protected $code = 0;
}

/**
 * Class SearchInChildrenException
 * Restrikce podle hodnoty v elementu bez hodnoty.
 */
class SearchInChildrenException extends BadInputFileFormatException
{
    /**
     * @var int
     */
    protected $code = 4;
}