<?php

#XQR:xzurek13

/**
 * Načte parametry
 *
 * @property bool help
 * @property string input
 * @property string output
 * @property string query
 * @property bool skipHead
 * @property string root
 */
class ParameterLoader
{
    /**
     * @var bool
     */
    public $help = false;

    /**
     * @var string
     */
    public $input = 'php://stdin';

    /**
     * @var string
     */
    public $output = 'php://stdout';

    /**
     * @var string
     */
    public $query = '';

    /**
     * @var bool
     */
    public $skipHead = false;

    /**
     * @var string
     */
    public $root = '';

    /**
     * Načte a parsuje parametry
     *
     * @param array $argv
     *
     * @throws InvalidArgumentException
     * @throws InputFileNotFoundException
     */
    public function __construct (array $argv)
    {
        $options = getopt('n', array(
            'help',
            'input:',
            'output:',
            'query:',
            'qf:',
            'root:',
        ));

        if (isset($options['n']) && array_search('-n', $argv) === false) {  // ve zkrácené formě např. z parametru "-input" vybere -n
            throw new BadArgumentException('Parametr -n záhadně zadán.');
        }

        if (isset($options['help']) && array_search('--help', $argv) === false) {
            throw new BadArgumentException('Parametr --help zadán s hodnotou.');
        }

        if (count($options) != count($argv) - 1) {
            throw new BadArgumentException('Počet parametrů z getopt nesedí s počtem parametrů v argv.');
        }

        if (isset($options['help']) && count($argv) != 2) {
            throw new BadArgumentException('Zadán parametr --help spolu s dalšími parametry.');
        }

        if (isset($options['query']) && isset($options['qf'])) {
            throw new BadArgumentException('Zadán parametr --query a zároveň --qf. Zvolte pouze jeden z nich.');
        }

        if (isset($options['qf']) && !is_readable($options['qf'])) {
            throw new BadQueryFileException('Soubor zadaný v parametru --qf nebyl nalezen nebo není čitelný.');
        }

        if (!isset($options['help']) && !isset($options['qf']) && !isset($options['query'])) {
            throw new BadArgumentException('Nebyl zadán parametr --qf ani --query.');
        }

        if (isset($options['input']) && !is_readable($options['input'])) {
            throw new InputFileNotFoundException('Soubor zadaný v parametru --input nebyl nalezen nebo není čitelný');
        }

        if (isset($options['output']) && file_put_contents($options['output'], '') === false) {
            throw new OutputFileNotWriteableException('Soubor zadaný v parametru --output není zapisovatelný.');
        }

        $this->parse($options);
    }

    /**
     * Parsuje parametry a vloží je do properties
     *
     * @param array $options
     */
    public function parse (array $options)
    {
        if (isset($options['help'])) {
            $this->help = true;
        }

        if (isset($options['input'])) {
            $this->input = $options['input'];
        }

        if (isset($options['output'])) {
            $this->output = $options['output'];
        }

        if (isset($options['query'])) {
            $this->query = $options['query'];
        } elseif (isset($options['qf'])) {
            $this->query = file_get_contents($options['qf']);
        }

        if (isset($options['n'])) {
            $this->skipHead = true;
        }

        if (isset($options['root'])) {
            $this->root = $options['root'];
        }
    }
}

/**
 * Class BadArgumentException
 * Špatně zadaný parametr nebo kombinace parametrů.
 */
class BadArgumentException extends Exception
{
    /**
     * @var int
     */
    protected $code = 1;
}

/**
 * Class InputFileNotFoundException
 * Soubor nenalezen nebo není čitelný.
 */
class InputFileNotFoundException extends BadArgumentException
{
    /**
     * @var int
     */
    protected $code = 2;
}

/**
 * Class OutputFileNotReadableException
 * Výstupní soubor nelze otevřít pro zápis.
 */
class OutputFileNotWriteableException extends BadArgumentException
{
    /**
     * @var int
     */
    protected $code = 3;
}

/**
 * Class EmptyQueryFileException
 * Soubor --qf je nečitelný.
 */
class BadQueryFileException extends BadArgumentException
{
    /**
     * @var int
     */
    protected $code = 80;
}