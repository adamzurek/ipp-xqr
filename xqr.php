<?php

#XQR:xzurek13

/**
 * Projekt: IPP/XQR
 * Datum vydání: 2015-03-18
 * Autor: Adam Žurek, xzurek13
 */

@ini_set('display_errors', true);
error_reporting(-1);
setlocale(LC_ALL, array('cs_CZ.UTF-8', 'cs_CZ.utf8'));
mb_internal_encoding('UTF-8');

require_once 'Helper.class.php';
require_once 'ParameterLoader.class.php';
require_once 'QueryParser.class.php';
require_once 'XMLQuery.class.php';
require_once 'Where.class.php';

$params = null;

// zkusit vyparsovat parametry z argv
try {
    $params = new ParameterLoader($argv);
} catch (BadArgumentException $e) {
    Helper::error($e);
}

// vypsat nápovědu
if ($params->help) {
    Helper::printHelp();
}

// zkusit vyparsovat SQL dotaz
try {
    $sql = new QueryParser($params->query);
} catch (BadArgumentException $e) {
    Helper::error($e);
}

$query = null;

try {
    // vybrat prvek, na který budu dále aplikovat operace where, order, limit
    $query = new XMLQuery(file_get_contents($params->input), $sql->from);
} catch (BadInputFileFormatException $e) {
    Helper::error($e);
}

if ($sql->from) { // pokud je prázdný FROM tak nic v XML nehledat
    $query->select($sql->select);

    try {
        $query->where($sql->where);
    } catch (BadWhereException $e) {
        Helper::error($e);
    } catch (SearchInChildrenException $e) {
        Helper::error($e);
    }

    if ($sql->order) {
        $query->order($sql->order, $sql->sort);
    }

    $query->limit($sql->limit);
}

// uložit výsledné xml do souboru
file_put_contents($params->output, $query->toXml($params->skipHead, $params->root));
